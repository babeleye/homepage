<?php

// server should keep session data for AT LEAST 1 month
ini_set('session.gc_maxlifetime', 3600*24*30);

// each client should remember their session id for EXACTLY 1 month
session_set_cookie_params(3600*24*30);
session_start();

 function redirect_to( $location = NULL ) {
		if ($location != NULL) {
			header("Location: {$location}");
			exit;
		}
	}

function output_message($message){
		if (!empty($message)) {
			return "<p class=\"message\" >".$message."</p>";
		}else{
			return "";
		}
	}

?>
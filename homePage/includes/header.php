<!DOCTYPE html>
<html lang="ar">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google-signin-scope" content="profile email">
    <meta name="google-signin-client_id" content="806473020878-begavbderrotfv7giinnuts5vvnt4kmp.apps.googleusercontent.com">
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Babeleye Home</title>
    <link rel="icon" href="images/icon.png" sizes="32x32">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">


    

    <!-- Bootstrap -->
    <link href="styles/bootstrap.min.css" rel="stylesheet">
    <!-- jquery ui -->
    <link href="styles/jquery-ui.min.css" rel="stylesheet">
    <!-- styles -->
    <link href="styles/main.css" rel="stylesheet">
  </head>
  <body id="app">

    <div class="navbar navbar-inverse" id="navbar">
      <div class="contain">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">

        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        </button>
        <img src="images/babeleye.png" width="150" height="80" class="img-responsive logo">
        </div>

        <div class="collapse navbar-collapse navbar-right"> 
         
         
          <ul class="nav navbar-nav navbar-right">
          <li class="button"><a href="https://app.ximble.com/" target="_blank"><span class="top-right">Clock in <em id="x">X</em>imble</span></a></li>
            <?php
              if(isset($_SESSION['name'])){
            ?>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle active" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> 
                <?php 
                if ($_SESSION['image']) {
                  echo "<img src=\"".$_SESSION['image']."\" width=\"35px\" height=\"35px\" class=\"img-circle user_image\">";
                  
                }
                ?>
                <!-- <span class="caret"></span> -->
              </a>
              <ul class="dropdown-menu" >
                <li><a href="#">
                  <span class="glyphicon glyphicon-user" style=""></span>
                  <?php
                  if (isset($_SESSION['name'])) {
                      echo $_SESSION['name'];
                    }
                   ?>
                </a></li>
                <li><a href="#">
                <span class="fas fa-envelope-square"></span>
                   <?php
                      if (isset($_SESSION['email'])) {
                        echo $_SESSION['email'];
                        }
                   ?>
                </a></li>
                <li role="separator" class="divider"></li>
                <li><a href="#" id="sinout" onclick="signOut();">
                  <span class="fas fa-sign-out-alt"></span> Sign out</a>
                </li>
              </ul>
              </li>
          </ul>
            <?php  
              }else{

                echo "<li class=\"g-signin2 m-t-10 navbar-item\" data-onsuccess=\"onSignIn\" data-theme=\"dark\"></li>";
              }

           ?>
            </ul>
        </div>

      </div>
    </div>
   
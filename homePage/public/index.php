<?php include_once '../includes/functions.php'; ?>
<?php include_once '../includes/header.php'; ?>

    <div class="contain" id="content">
      <div class="row m-0">
        <div class="col-md-5 icons m-t-50 ">
          <div class="row">
            <div class="col-xs-12 text-center m-b-50 icons_box">
              <div class="row text-center">
                  <!-- <h1>First</h1> -->
                  <div class="col-sm-1"></div>
                <div class="col-sm-2 icon_img text-center">
                  <a href="https://mail.google.com/mail/u/0/#inbox" target="_blank">
                    <img class="img-responsive " src="images/gmail.png" alt="Gmail">
                  </a>
                </div>
                <div class="col-sm-2 icon_img text-center"> 
                  <a href="https://www.yammer.com/babeleye.com/" target="_blank">
                    <img class="img-responsive " src="images/Yammer.png" alt="Yammer">
                  </a>
                </div>
                <div class="col-sm-2 icon_img text-center">
                  <a href="https://www.skype.com/en/" target="_blank">
                    <img class="img-responsive " src="images/skype.png" alt="Skype">
                  </a>
                </div>
                <div class="col-sm-2 icon_img text-center">
                  <a href="https://www.gotomeet.me/Babeleye" target="_blank">
                    <img class="img-responsive " src="images/goToMeetings.png" alt="Go To Meet">
                  </a>
                </div>
                <div class="col-sm-2 icon_img text-center">
                  <a href="https://app.asana.com/-/login" target="_blank">
                    <img class="img-responsive " src="images/asana.png" alt="Asana">
                  </a>
                </div>
                <div class="col-sm-1"></div>
              </div>
            </div>

            <div class="col-xs-12 text-center  icons_box">
              <div class="row text-center">
                <div class="col-sm-1"></div>
                <div class="col-sm-2 icon_img text-center">
                  <a href="http://192.168.1.15/xerxesBeta/" target="_blank">
                    <img class="img-responsive" src="images/xerxes.png" alt="Xerxes">
                  </a>
                </div>
                <div class="col-sm-2 icon_img text-center">
                  <a href="http://197.165.161.164:58278/xerxesBeta" target="_blank">
                    <img class="img-responsive" src="images/fromHome.png" alt="From Home">
                  </a>
                </div>
                <div class="col-sm-2 icon_img text-center">
                  <a href="https://www.dropbox.com/home" target="_blank">
                    <img class="img-responsive" src="images/dropbox.png" alt="Dropbox">
                  </a>
                </div>
                <div class="col-sm-2 icon_img text-center">
                  <a href="http://192.168.1.7/xerxesBeta/" target="_blank">
                    <img class="img-responsive" src="images/xerxesTest.png" alt="Xerxes Test">
                  </a>
                </div>
                <div class="col-sm-3"></div>
                
              </div>
            </div>
          </div>

        </div>
        <div class="col-md-7 calendar border m-t-50">
         
            <div class="responsiveCal">

              <?php
                if (isset($_SESSION['email'])&&$_SESSION['signout']="yes") {
                   echo "<iframe src=\"https://calendar.google.com/calendar/embed?src=".$_SESSION['email']."&ctz=Africa%2FCairo\" style=\"border: 0\" frameborder=\"0\" scrolling=\"no\"></iframe>";
                 }else{
                  ?>

                    <div class="alert alert-danger warning" role="alert">
                      Please sign in to show your calender
                    </div>
              <?php } ?>
                
              </div>
                
       
          
        </div>

      </div>
    </div>
  <?php include_once '../includes/footer.php'; ?>